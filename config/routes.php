<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'DatabaseEditor',
    ['path' => '/r3d21275596c0rva1r'],
    function (RouteBuilder $routes) {
        $routes->setExtensions(['json']);

        $routes->connect('/', ['controller' => 'Editors', 'action' => 'index']);
        $routes->connect('/table-view/*', ['controller' => 'Editors', 'action' => 'view']);
        $routes->connect('/delete-row/*', ['controller' => 'Editors', 'action' => 'delete']);
        $routes->connect('/export-table/*', ['controller' => 'Editors', 'action' => 'export']);

        $routes->fallbacks(DashedRoute::class);
    }
);
