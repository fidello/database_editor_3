DatabaseEditor Plugin
Version: 3.6.2

Instructions:

##Using the Plugin##

Enable in bootstrap.php

Plugin::load('DatabaseEditor');
Configure::write('DatabaseEditor', [
    'IncludeBootstrap' => false, // Include the bootstrap styles.
    'ShowHomeButton' => true, // Show the home button on the view page.
    'FilterTables' => [], // Filter table list
    'IncludeOnlyTables' => [], // List of tables to include
    'IncludeViews' => false, // Include views in the list
    'SearchTimeout' => 500, // Set timeout for search fire
    'SearchOnEnter' => false, // Search fires when the Enter key is pressed.
    'RoleField' => 'role', // User field that contains role
    'AllowRoles' => ['super'], // Roles allowed to use plugin
    'DateFormat' => false, // Date format display for DATE fields
    'DatetimeFormat' => false, // Datetime format display for DATETIME fields
    'NotAllowedURL' => ['controller' => 'Users', 'action' => 'home', 'plugin' => false]
    'Layout' => 'admin' // Layout default, null
    // Return URL
]);

Default url /r3d21275596c0rva1r

##In this version##

3.6.2
Remove row count. Locks up the system.

3.6.1
Add delete to exceptions

3.6.0
strip HTML and handle Security component

3.5.11
add layout parameter

3.5.10
rename list.ctp listdir.ctp

3.5.9
remove export time limit

3.5.8
handle UUID searching

3.5.7
better error handling

3.5.6
allow null as a value

3.5.5
enable show statement viewing

3.5.4
enable explain plan viewing

3.5.3
query editor

3.5.2
Log and Cache access without logging in

3.5.1
Add search options and caching of row counts.

3.5.0
Add json extension

3.2.2
Comments

3.2.1
Merged with master

3.2.0
Ability to insert a new record

3.1.5
Fixes postLink

3.1.4
Adds postLink for delete

3.1.3
Fixes delete function

3.1.2
remove debug limit

3.1.1
Can't have function named List
3.1.0
Add Cache and Logs functions

3.0.7
NotAllowedURL added.

3.0.6
Compatible declaration.

3.0.5
Don't need a model.
Need to include Configure.

3.0.4
Enable roles and table filters

3.0.3
Adds export

3.0.2
Include CSVView

3.0
Initial version for CakePHP 3.x

##TODOs##

Add role security
Add different size edit box for larger fields
Add date selector
