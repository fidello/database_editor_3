<?php

namespace DatabaseEditor\Controller;

use DatabaseEditor\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Database\Schema\Collection;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Event\Event;
use BlueM\SearchstringParser;

/**
 * Editors Controller
 *
 */
class EditorsController extends AppController
{
    /**
     * beforeFilter method
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        $role = Configure::read('DatabaseEditor.RoleField') ? Configure::read('DatabaseEditor.RoleField') : 'role';
        $allowed = Configure::read('DatabaseEditor.AllowRoles') ? Configure::read('DatabaseEditor.AllowRoles') : ['admin', 'super'];
        if (!in_array($this->Auth->user($role), $allowed)) {
            $this->Flash->error('Not Authorized');
            $home = Configure::read('DatabaseEditor.NotAllowedURL');
            if ($home) {
                return $this->redirect($home);
            }
            $this->redirect(['controller' => 'Users', 'action' => 'home', 'plugin' => false]);
        }

        // Ignore _Token on update
        if (isset($this->Security)) {
            $this->Security->setConfig('unlockedActions', ['update', 'delete']);
        }
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // list of tables
        $db = ConnectionManager::get('default');
        $collection = $db->schemaCollection();
        $tables = $collection->listTables();
        $filter = Configure::read('DatabaseEditor.FilterTables');
        $include = Configure::read('DatabaseEditor.IncludeOnlyTables');
        $views = Configure::read('DatabaseEditor.IncludeViews');
        if ($filter) {
            $tables = preg_grep($filter, $tables);
        }
        if ($include) {
            if (!is_array($include)) {
                $include = [$include];
            }
            $tables = array_intersect($tables, $include);
        }
        $this->set(compact('tables'));
    }

    /**
     * View method
     *
     * @param string $table Table name
     * @return \Cake\Network\Response|null
     */
    public function view($table)
    {
        $obj = TableRegistry::getTableLocator()->get($table);
        $schema = $obj->schema();
        $primaryKey = $obj->getPrimaryKey();
        $this->set(compact('table', 'schema', 'primaryKey'));
    }

    /**
     * Add method
     *
     * @param string $table Table name
     * @return \Cake\Network\Response|null
     */
    public function add($table)
    {
        $tableObj = TableRegistry::getTableLocator()->get($table);
        $schema = $tableObj->schema();
        $entity = $tableObj->newEntity();
        if ($this->request->is('post')) {
            $entity = $tableObj->patchEntity($entity, $this->request->getData());
            if ($tableObj->save($entity)) {
                $this->Flash->success(__('The record has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The record could not be saved. Please, try again.'));
        }
        $this->set(compact('table', 'schema', 'entity'));
    }

    /**
     * data method
     *
     * @param string $table Table name
     * @return void
     */
    public function data($tableName)
    {
        $table = TableRegistry::getTableLocator()->get($tableName);
        $primaryKey = $table->getPrimaryKey();
        $query = $table->find()->select([$primaryKey]);
        $options = $this->request->query;
        $query->limit(isset($options['limit']) ? $options['limit'] : 20);
        $query->offset(isset($options['offset']) ? $options['offset'] : 0);
        $query->order(
            (isset($options['sort']) ? $options['sort'] : 'id')
                . ' ' .
                (isset($options['order']) ? $options['order'] : 'asc')
        );
        if (isset($options['filter'])) {
            $filter = json_decode($options['filter']);
            foreach ($filter as $field => $value) {
                // dump($value);
                $search = new SearchstringParser($value);
                $orTerms = $search->getOrTerms();
                $andTerms = $search->getAndTerms();
                if ($andTerms) {
                    foreach ($andTerms as $value) {
                        $andExp[] = $this->_getExp($field, $value);
                    }
                }
                if ($orTerms) {
                    foreach ($orTerms as $value) {
                        $orExp[] = $this->_getExp($field, $value);
                    }
                    $expression['OR'] = $orExp;
                    if (isset($andExp)) {
                        $expression['OR'][] = $andExp;
                    }
                }
                $query->where($expression);
            }
        }

        $this->set([
            'rows' => $table->find()->innerJoin(
                ['Sub' => $query],
                ['Sub.' . $tableName . '__' . $primaryKey . '=' . $tableName . '.' . $primaryKey]
            ),
            'total' => $query->count(),
            '_serialize' => ['total', 'rows']
        ]);
    }

    /**
     * Get the expression clause for query
     *
     * @return Closure
     */
    protected function _getExp($field, $value)
    {
        if ($value == 'null') {
            $exp = function ($exp) use ($field) {
                return $exp->isNull($field);
            };
        } elseif ($value == 'empty') {
            $exp = function ($exp) use ($field) {
                return $exp->eq($field, '');
            };
        } elseif ($value == 'notnull') {
            $exp = function ($exp) use ($field) {
                return $exp->isNotNull($field);
            };
        } elseif (strpos($value, ',') !== false) {
            $list = explode(',', $value);
            $exp = function ($exp) use ($field, $list) {
                return $exp->in($field, $list);
            };
        } elseif (strpos($value, '--') !== false) {
            $list = explode('--', $value);
            $exp = function ($exp) use ($field, $list) {
                return $exp->between($field, $list[0], $list[1]);
            };
        } elseif (strpos($value, '<=') !== false) {
            $list = str_replace('<=', '', $value);
            $exp = function ($exp) use ($field, $list) {
                return $exp->lte($field, $list);
            };
        } elseif (strpos($value, '<') !== false) {
            $list = str_replace('<', '', $value);
            $exp = function ($exp) use ($field, $list) {
                return $exp->lt($field, $list);
            };
        } elseif (strpos($value, '>=') !== false) {
            $list = str_replace('>=', '', $value);
            $exp = function ($exp) use ($field, $list) {
                return $exp->gte($field, $list);
            };
        } elseif (strpos($value, '>') !== false) {
            $list = str_replace('>', '', $value);
            $exp = function ($exp) use ($field, $list) {
                return $exp->gt($field, $list);
            };
        } else {
            $exp = function ($exp) use ($field, $value) {
                return $exp->like($field, $value);
            };
        }

        return $exp;
    }

    /**
     * update method
     *
     * @param string $table Table name
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function update($table)
    {
        $this->autoRender = false;

        $tableObject = TableRegistry::getTableLocator()->get($table);
        $schema = $tableObject->schema();

        $name = $this->request->getData('name');
        $value = strip_tags($this->request->getData('value'));

        // Format date and datetime for database
        $type = $schema->baseColumnType($name);
        if ($type == 'date') {
            $tmp = strtotime($value);
            if ($tmp !== false || $tmp > 0) {
                $value = date('Y-m-d', $tmp);
            }
        }
        if ($type == 'datetime') {
            $tmp = strtotime($value);
            if ($tmp !== false || $tmp > 0) {
                $value = date('Y-m-d H:i:s', $tmp);
            }
        }

        // This may be a view
        if (is_null($schema->options()['engine'])) {
            $tableObject->removeBehavior('Timestamp');
        }

        $data = $tableObject->get($this->request->getData('pk'));
        if (strtoupper($value) == 'NULL') {
            $data->$name = null;
        } else {
            $data->$name = $value;
        }

        try {
            $status = $tableObject->save($data, ['checkRules' => false]);
        } catch (\Exception $e) {
            return $this->response->withStatus(500)->withStringBody($e->getMessage());
        }

        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    /**
     * export method
     *
     * @param string $table Table name
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function export($table)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        $tableObject = TableRegistry::get($table);
        $schema = $tableObject->schema();
        $entity = $tableObject->newEntity();
        $hidden = $entity->hiddenProperties();
        $columns = $schema->columns();
        $_header = $_extract = array_diff($columns, $hidden);
        $_serialize = 'rows';

        $rows = $tableObject->find()->bufferResults(false);
        $this->viewBuilder()->className('CsvView.Csv');
        $this->response->download($table . '.csv');
        $this->set(compact('rows', '_serialize', '_header', '_extract'));
    }

    /**
     * Delete method
     *
     * @param string $table Table name
     * @param string|null $id Editor id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($table, $id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tableObject = TableRegistry::get($table);
        $record = $tableObject->get($id);
        if ($tableObject->delete($record)) {
            $this->Flash->success(__('Record deleted.'));
        } else {
            $this->Flash->error(__('Record was not deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'view', $table]);
    }

    /**
     * query method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function query()
    {
        $query = '';
        if ($this->request->is(['post', 'put'])) {
            $db = ConnectionManager::get('default');
            $query = $this->request->getData('query');
            try {
                $this->set('stmt', $db->query($query));
            } catch (\Exception $e) {
                $this->set('error', $e->getMessage());
            }
        }
        $this->set(compact('query'));
    }
}
