<?php

namespace DatabaseEditor\Controller;

use Cake\Cache\Cache;
use Cake\Cache\Engine\ApcEngine;
use Cake\Event\Event;

/**
 * Caches Controller
 *
 */
class CachesController extends AppController
{
    /**
     * beforeFilter method
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow();
        parent::beforeFilter($event);
    }

    /**
     * Clear metadata.
     *
     * @param string|null $prefix The cache prefix to be cleared.
     * @throws \Cake\Console\Exception\StopException
     * @return void
     */
    public function clear($prefix = null)
    {
        try {
            $engine = Cache::engine($prefix);
            Cache::clear(false, $prefix);
            if ($engine instanceof ApcEngine) {
                $this->Flash->info("ApcEngine detected: Cleared $prefix CLI cache successfully " .
                "but $prefix web cache must be cleared separately.");
            } elseif ($engine instanceof WincacheEngine) {
                $this->Flash->info("WincacheEngine detected: Cleared $prefix CLI cache successfully " .
                "but $prefix web cache must be cleared separately.");
            } else {
                $this->Flash->success("Cleared $prefix cache");
            }
        } catch (\InvalidArgumentException $e) {
            $this->Flash->error($e->getMessage());
        }
    }

    /**
     * Clear metadata.
     *
     * @return void
     */
    public function clearAll()
    {
        $prefixes = Cache::configured();
        foreach ($prefixes as $prefix) {
            $this->clear($prefix);
        }
    }

    /**
     * Show a list of all defined cache prefixes.
     *
     * @return void
     */
    public function listing()
    {
        $prefixes = Cache::configured();
        $this->set(compact('prefixes'));
    }
}
