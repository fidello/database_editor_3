<?php

namespace DatabaseEditor\Controller;

use App\Controller\AppController as BaseController;
use Cake\Core\Configure;
use Cake\Event\Event;

class AppController extends BaseController
{

    /**
     * beforeFilter method
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        $layout = Configure::read('DatabaseEditor.Layout');
        if ($layout) {
            $this->viewBuilder()->setLayout($layout);
        }

        parent::beforeFilter($event);
    }
}
