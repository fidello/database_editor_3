<?php

namespace DatabaseEditor\Controller;

use Cake\Log\Log;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Event\Event;

/**
 * Logs Controller
 *
 */
class LogsController extends AppController
{
    /**
     * beforeFilter method
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow();
        parent::beforeFilter($event);
    }

    /**
     * getlog method
     *
     * @return \Cake\Network\Response|null
     */
    public function get($filename)
    {
        $log = Log::config('error');
        $dir = new Folder($log['path']);
        $files = $dir->find($filename);
        $contents = 'File not found.';
        if ($files) {
            $file = new File($dir->pwd() . DS . $files[0]);
            $contents = $file->read();
        }
        $this->set(compact('contents', 'filename'));
    }

    /**
     * Delete log method
     *
     * @return \Cake\Network\Response|null
     */
    public function delete($filename)
    {
        $log = Log::config('error');
        $dir = new Folder($log['path']);
        $files = $dir->find($filename);
        if ($files) {
            unlink($dir->pwd() . DS . $files[0]);
        }
        $this->redirect(['action' => 'get', $filename]);
    }

    /**
     * Download method
     *
     * @return \Cake\Network\Response|null
     */
    public function download($directory, $filename = null)
    {
        if (is_null($filename)) {
            $filename = $directory;
            $log = Log::config('error');
        } else {
            $log['path'] = realpath('../' . $directory);
        }
        $dir = new Folder($log['path']);
        $files = $dir->find($filename);
        $this->response->file($dir->pwd() . DS . $files[0]);
        // Return the response to prevent controller from trying to render
        // a view.
        return $this->response;
    }

    /**
     * Show a list of all defined Log prefixes.
     *
     * @return \Cake\Network\Response|null
     */
    public function listdir($directory = null)
    {
        if (is_null($directory)) {
            $log = Log::config('error');
            $directory = 'logs';
        } else {
            $log['path'] = realpath('../' . $directory);
        }
        $dir = new Folder($log['path']);
        $files = $dir->find('.*');
        $this->set(compact('files', 'directory'));
    }
}
