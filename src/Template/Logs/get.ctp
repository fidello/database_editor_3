<?php
$errors = ['\[.*Exception\]', 'Fatal Error', 'Error:'];
$lines = ['\[.*line.*\]', '#[0-9]*'];
$urls = ['Request URL: .*', 'Stack Trace:'];
$dates = ['[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]'];
$contents = htmlentities($contents);
$contents = preg_replace("/(" . implode('|', $dates) . ")/", '<hr><span style="font-weight:bold;">$1</span>', $contents);
$contents = preg_replace("/(" . implode('|', $urls) . ")/", '<span style="color:brown;">$1</span>', $contents);
$contents = preg_replace("/(" . implode('|', $lines) . ")/", '<span style="color:green;font-weight:bold">$1</span>', $contents);
$contents = preg_replace("/(" . implode('|', $errors) . ")/", '<span style="color:red;font-weight:bold;">$1</span>', $contents);
?>
<div class="float-right">
	<?= $this->Html->link('Download', ['action' => 'download', $filename], ['class' => 'btn btn-primary']); ?>
    <?= $this->Form->postLink('Delete', ['action' => 'delete', $filename], ['class' => 'btn btn-danger', 'confirm' => sprintf('Delete %s?', $filename)]); ?>
</div>
<h2><?= $filename; ?></h2>
<div class="card">
    <div class="card-body">
        <?= nl2br($contents); ?>
    </div>
</div>
