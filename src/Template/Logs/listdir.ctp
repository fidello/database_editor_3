<h2>Log Files</h2>

<ul>
<?php foreach ($files as $file): ?>
    <li><?= $this->Html->link($file, ['action' => 'download', $directory, $file]) ?></li>
<?php endforeach; ?>
</ul>
