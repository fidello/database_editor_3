<h2>Results</h2>
<?php if (isset($error)): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $error ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>

<?= $this->Form->create() ?>
<?= $this->Form->control('query', ['type' => 'textarea', 'rows' => 3, 'value' => $query]) ?>
<?= $this->Form->button('Submit', ['class' => 'btn btn-primary']) ?>
<?= $this->Form->end() ?>

<?php if (isset($stmt)): ?>
    <?php $count = $stmt->rowCount() ?>
    Rows: <strong><?= number_format($count) ?></strong>
    <?php if ($count > 20): ?>
        <em>(Showing first 20)</em>
    <?php endif; ?>
    <?php if (stripos(trim($query), 'SELECT') !== false || stripos(trim($query), 'SHOW') !== false): ?>
        <table class="table table-hover table-striped table-responsive mt-3">
        <?php for ($i = 0; $i < min(20, $count); $i++): $row = $stmt->fetch('assoc'); ?>
            <?php if ($i == 0): ?>
                <thead>
                <tr>
                    <?php foreach ($row as $field => $value): ?>
                        <th><?= $field ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>
            <?php endif ?>
            <tr>
                <?php foreach ($row as $field => $value): ?>
                    <td><?= h($value) ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endfor; ?>
        </table>
    <?php endif; ?>
<?php endif; ?>
