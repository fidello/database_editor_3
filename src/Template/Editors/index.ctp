<?php

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;

?>
<h2>Table List</h2>
<table class="table table-hover table-striped">
    <tr>
        <th>Name</th>
        <th>Actions</th>
    </tr>
    </tr>
    <?php foreach ($tables as $table) : ?>
        <tr>
            <td>
                <?= $this->Html->link($table, ['action' => 'view', $table]) ?>
            </td>
            <?php if (0) : ?>
                <td class="align-right">
                    ~<?= number_format(
                            Cache::remember('db_cnt_' . $table, function () use ($table) {
                                return TableRegistry::get($table)->find()->count();
                            }, 'database_editor')
                        ) ?>
                </td>
            <?php endif; ?>
            <td>
                <?= $this->Html->link('<i class="fa fa-download"></i>', ['action' => 'export', $table], ['class' => 'btn btn-success', 'escape' => false]) ?>
                <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add', $table], ['class' => 'btn btn-secondary', 'escape' => false]) ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>