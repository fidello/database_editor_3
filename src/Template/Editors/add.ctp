<?php
use Cake\Core\Configure;
$this->assign('title', $table);
?>
<h2>
    New <?= $table ?>
    <?php if (!Configure::check('DatabaseEditor.ShowHomeButton') || (Configure::check('DatabaseEditor.ShowHomeButton') && Configure::read('DatabaseEditor.ShowHomeButton'))): ?>
        <?= $this->Html->link('<i class="fa fa-home"></i>', ['action' => 'index'], ['class' => 'btn btn-primary', 'escape' => false]); ?>
    <?php endif; ?>
    <?= $this->Html->link('<i class="fa fa-table"></i>', ['action' => 'view', $table], ['class' => 'btn btn-secondary', 'escape' => false]); ?>
</h2>
<?= $this->Form->create($entity) ?>
    <?php foreach ($schema->columns() as $name): ?>
        <?php if (in_array($name, ['id', 'created', 'modified'])) continue; ?>
        <?php if ($schema->baseColumnType($name) == 'text'): ?>
            <?= $this->Form->control($name, ['rows' => 3]) ?>
        <?php else: ?>
            <?= $this->Form->control($name, ['type' => 'text']) ?>
        <?php endif; ?>
    <?php endforeach; ?>
    <button class="btn btn-primary btn-lg">Save</button>
<?= $this->Form->end() ?>