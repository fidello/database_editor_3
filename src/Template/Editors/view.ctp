<?php

use Cake\Core\Configure;

$this->assign('title', $table);
?>
<h2>
    <?= $table ?>
    <?php if (!Configure::check('DatabaseEditor.ShowHomeButton') || (Configure::check('DatabaseEditor.ShowHomeButton') && Configure::read('DatabaseEditor.ShowHomeButton'))) : ?>
        <?= $this->Html->link('<i class="fa fa-home"></i>', ['action' => 'index'], ['class' => 'btn btn-primary', 'escape' => false]); ?>
    <?php endif; ?>
    <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add', $table], ['class' => 'btn btn-secondary', 'escape' => false]) ?>
    <?= $this->Html->link('<i class="fa fa-download"></i>', ['action' => 'export', $table], ['class' => 'btn btn-info', 'escape' => false, 'title' => 'export ' . $table]); ?>
</h2>

<h3>Search options <span class="option-show"><i class="fa fa-plus"></i></span></h3>
<div class="options" style="display:none;">
    <div class="row">
        <div class="col-6">
            <dl class="row">
                <dd class="col-2">%</dd>
                <dt class="col-10">wildcard</dt>
                <dd class="col-2">NULL</dd>
                <dt class="col-10">return values that are NULL</dt>
                <dd class="col-2">NOTNULL</dd>
                <dt class="col-10">return values that are NOT NULL</dt>
                <dd class="col-2">EMPTY</dd>
                <dt class="col-10">return values that are EMPTY (empty string)</dt>
                <dd class="col-2">x,y</dd>
                <dt class="col-10">returns values are x or y, etc. (cannot contain spaces)</dt>
            </dl>
        </div>
        <div class="col-6">
            <dl class="row">
                <dd class="col-1">&lt;x</dd>
                <dt class="col-11">less than x (cannot contain spaces)</dt>
                <dd class="col-1">&lt;=x</dd>
                <dt class="col-11">less than or equal x (cannot contain spaces)</dt>
                <dd class="col-1">&gt;x</dd>
                <dt class="col-11">greater than x (cannot contain spaces)</dt>
                <dd class="col-1">&gt;=x</dd>
                <dt class="col-11">greater than or equal x (cannot contain spaces)</dt>
                <dd class="col-1">x--y</dd>
                <dt class="col-11">returns values between x and y (double minus, cannot contain spaces)</dt>
            </dl>
        </div>
    </div>
</div>
<?php
// options
$searchTimeout = Configure::read('DatabaseEditor.SearchTimeout');
$searchOnEnter = Configure::read('DatabaseEditor.searchOnEnter');

// cdn versions
$tbs = '4.4.1';
$btab = '1.15.5';
$xedit = '1.5.1';
$fawe = '5.0.13';
$headers = [];
foreach ($schema->columns() as $field) {
    $type = $schema->baseColumnType($field) == 'text' ? 'textarea' : 'text';
    if (in_array($field, ['id', 'created', 'modified'])) {
        $headers[] = [
            $field => [
                'data-field' => $field,
                'data-sortable' => 'true',
                'data-filter-control' => 'input',
                'data-valign' => 'top',
            ]
        ];
    } else {
        $headers[] = [
            $field => [
                'data-field' => $field,
                'data-sortable' => 'true',
                'data-filter-control' => 'input',
                'data-valign' => 'top',
                'data-editable' => 'true',
                'data-editable-type' => $type,
                'data-escape' => 'true'
            ]
        ];
    }
}
$headers[] = [
    'Delete' => [
        'data-field' => 'id_save',
        'data-formatter' => 'deleteButton',
        'data-valign' => 'top',
    ]
];
?>

<table id="table" data-toggle="table" data-url="<?= $this->Url->build(['action' => 'data', $table . '.json']); ?>" data-pagination="true" data-side-pagination="server" data-page-size="20" data-page-list="[5, 10, 20, 50, 100, 200, All]" data-striped="true" data-filter-control="true" data-id-field="<?= $primaryKey ?>" data-editable-url="<?= $this->Url->build(['action' => 'update', $table]) ?>" data-editable-mode="inline" data-sort-name="<?= $primaryKey ?>" data-sort-order="asc" data-search-time-out="<?= $searchTimeout ? $searchTimeout : 1000 ?>" <?php if ($searchOnEnter) : ?> data-search-on-enter-key="true" <?php endif; ?>>
    <thead>
        <?= $this->Html->tableHeaders($headers); ?>
    </thead>
</table>

<?php if ($schema->indexes()) : ?>
    <h3>Indexes</h3>

    <table class="table">
        <tr>
            <th>Name</th>
            <th>Columns</th>
            <th>Type</th>
        </tr>
        <?php foreach ($schema->indexes() as $name) : ?>
            <tr>
                <th>
                    <?= $name; ?>
                </th>
                <?php $attribs = $schema->index($name) ?>
                <?php if (is_array($attribs['columns'])) : ?>
                    <td><?= implode(', ', $attribs['columns']); ?></td>
                <?php else : ?>
                    <td><?= $attribs['column']; ?></td>
                <?php endif; ?>
                <td><?= $attribs['type'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php endif; ?>


<?php
if (Configure::read('DatabaseEditor.IncludeBootstrap')) {
    $this->Html->css([
        "//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/$tbs/css/bootstrap.min.css",
    ], ['block' => true]);
}
$this->Html->css([
    "//cdnjs.cloudflare.com/ajax/libs/font-awesome/$fawe/css/font-awesome.min.css",
    "//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/$btab/bootstrap-table.min.css",
    "//cdnjs.cloudflare.com/ajax/libs/x-editable/$xedit/bootstrap3-editable/css/bootstrap-editable.css"
], ['block' => true]);
$this->Html->script([
    "//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/$btab/bootstrap-table.min.js",
    "//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/$btab/extensions/filter-control/bootstrap-table-filter-control.min.js",
    "//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/$btab/extensions/editable/bootstrap-table-editable.min.js",
    "//cdnjs.cloudflare.com/ajax/libs/x-editable/$xedit/bootstrap3-editable/js/bootstrap-editable.min.js",
], ['block' => true]);
?>
<?php $this->Html->scriptStart(['block' => true]); ?>
$.fn.editableform.buttons =
'<button type="submit" class="btn btn-primary btn-sm editable-submit">'+
    '<i class="fa fa-fw fa-check"></i>'+
    '</button>'+
'<button type="button" class="btn btn-secondary btn-sm editable-cancel">'+
    '<i class="fa fa-fw fa-times"></i>'+
    '</button>';
function deleteButton(value, row) {
const id = row['id'];
const url = '<?= $this->Url->build(['action' => 'delete', $table, 'ID']) ?>'.replace('ID', id);
const formName = 'post_' + Math.floor((Math.random() * 10000000) + 1);
const postLink = '<form name="' + formName + '" style="display:none;" method="post" action="' + url + '"><input type="hidden" name="_method" value="DELETE" /></form><a href="#" class="btn btn-danger" onclick="if (confirm(&quot;Are you sure you want to delete this record? This cannot be undone.&quot;)) { document.' + formName + '.submit(); } event.returnValue = false; return false;"><i class="fa fa-trash"></i></a>';

return postLink;
}
$('.option-show').on('click', function() {
$('.options').toggle();
});
<?php $this->Html->scriptEnd(); ?>