<?php
namespace DatabaseEditor\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use DatabaseEditor\Model\Table\UsersTable;

/**
 * DatabaseEditor\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \DatabaseEditor\Model\Table\UsersTable
     */
    public $Users;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.database_editor.users',
        'plugin.database_editor.managers',
        'plugin.database_editor.projects',
        'plugin.database_editor.conversations',
        'plugin.database_editor.feedback',
        'plugin.database_editor.goal_comments',
        'plugin.database_editor.goals',
        'plugin.database_editor.impacts',
        'plugin.database_editor.nineboxes',
        'plugin.database_editor.privacy_optin',
        'plugin.database_editor.profiles',
        'plugin.database_editor.reviews',
        'plugin.database_editor.talent_answers',
        'plugin.database_editor.thoughts',
        'plugin.database_editor.user_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => 'DatabaseEditor\Model\Table\UsersTable'];
        $this->Users = TableRegistry::get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
